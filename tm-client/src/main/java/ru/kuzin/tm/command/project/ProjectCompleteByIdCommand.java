package ru.kuzin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.dto.request.ProjectCompleteByIdRequest;
import ru.kuzin.tm.util.TerminalUtil;

public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    @NotNull
    private static final String NAME = "project-complete-by-id";

    @NotNull
    private static final String DESCRIPTION = "Complete project by id.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(getToken());
        request.setId(id);
        getProjectEndpoint().completeProjectById(request);
    }

}