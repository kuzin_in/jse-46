package ru.kuzin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.api.endpoint.IProjectEndpoint;
import ru.kuzin.tm.dto.request.ProjectCreateRequest;
import ru.kuzin.tm.dto.response.ProjectCreateResponse;
import ru.kuzin.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @NotNull
    private static final String NAME = "project-create";

    @NotNull
    private static final String DESCRIPTION = "Create new project.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(getToken());
        request.setName(name);
        request.setDescription(description);
        @NotNull final IProjectEndpoint endpoint = getProjectEndpoint();
        @NotNull final ProjectCreateResponse req = endpoint.createProject(request);
    }

}