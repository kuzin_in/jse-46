package ru.kuzin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.dto.request.ProjectShowByIdRequest;
import ru.kuzin.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @NotNull
    private static final String NAME = "project-show-by-id";

    @NotNull
    private static final String DESCRIPTION = "Display project by id.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectShowByIdRequest request = new ProjectShowByIdRequest(getToken());
        request.setId(id);
        getProjectEndpoint().showProjectById(request);
    }

}