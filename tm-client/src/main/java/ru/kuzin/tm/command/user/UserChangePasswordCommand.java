package ru.kuzin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.dto.request.UserChangePasswordRequest;
import ru.kuzin.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "change-user-password";

    @NotNull
    private static final String DESCRIPTION = "change password of current user";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(getToken());
        request.setPassword(password);
        getUserEndpoint().changeUserPassword(request);
    }

}