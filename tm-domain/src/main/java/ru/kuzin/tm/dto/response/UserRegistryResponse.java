package ru.kuzin.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.dto.model.UserDTO;

@Getter
@Setter
@NoArgsConstructor
public final class UserRegistryResponse extends AbstractUserResponse {

    public UserRegistryResponse(@Nullable final UserDTO user) {
        super(user);
    }

}