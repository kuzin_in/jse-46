package ru.kuzin.tm.exception.user;

public final class AuthenticationException extends AbstractUserException {

    public AuthenticationException() {
        super("Error! Incorrect login or password was entered. Please try again...");
    }

}