package ru.kuzin.tm.api.repository.dto;

import ru.kuzin.tm.dto.model.ProjectDTO;

public interface IProjectDtoRepository extends IUserOwnedDtoRepository<ProjectDTO> {
}