package ru.kuzin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.api.component.ISaltProvider;

public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getApplicationConfig();

    @NotNull
    String getApplicationLogs();

    @NotNull
    String getApplicationName();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    String getGitBranch();

    @NotNull
    String getGitCommitId();

    @NotNull
    String getGitCommitterName();

    @NotNull
    String getGitCommitterEmail();

    @NotNull
    String getGitCommitMessage();

    @NotNull
    String getGitCommitTime();

    @NotNull
    Integer getServerPort();

    @NotNull
    String getServerHost();

    @NotNull
    String getSessionKey();

    @NotNull
    Integer getSessionTimeout();

    @NotNull
    String getDBUrl();

    @NotNull
    String getDBPassword();

    @NotNull
    String getDBUser();

    @NotNull
    String getDBSchema();

    @NotNull
    String getDBDriver();

    @NotNull
    String getDBL2Cache();

    @NotNull
    String getDBDialect();

    @NotNull
    String getDBShowSQL();

    @NotNull
    String getDBHbm2DDL();

    @NotNull
    String getDBCacheRegion();

    @NotNull
    String getDBQueryCache();

    @NotNull
    String getDBMinimalPuts();

    @NotNull
    String getDBCacheRegionPrefix();

    @NotNull
    String getDBCacheProvider();

}