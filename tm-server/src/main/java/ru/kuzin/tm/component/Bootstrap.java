package ru.kuzin.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.api.endpoint.*;
import ru.kuzin.tm.api.service.*;
import ru.kuzin.tm.api.service.dto.*;
import ru.kuzin.tm.api.service.model.IProjectService;
import ru.kuzin.tm.api.service.model.IProjectTaskService;
import ru.kuzin.tm.api.service.model.ITaskService;
import ru.kuzin.tm.api.service.model.IUserService;
import ru.kuzin.tm.endpoint.*;
import ru.kuzin.tm.service.AuthService;
import ru.kuzin.tm.service.ConnectionService;
import ru.kuzin.tm.service.LoggerService;
import ru.kuzin.tm.service.PropertyService;
import ru.kuzin.tm.service.dto.*;
import ru.kuzin.tm.service.model.ProjectService;
import ru.kuzin.tm.service.model.ProjectTaskService;
import ru.kuzin.tm.service.model.TaskService;
import ru.kuzin.tm.service.model.UserService;
import ru.kuzin.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService(propertyService);

    @NotNull
    @Getter
    private final IProjectDtoService projectDtoService = new ProjectDtoService(connectionService);

    @NotNull
    @Getter
    private final ITaskDtoService taskDtoService = new TaskDtoService(connectionService);

    @NotNull
    @Getter
    private final IUserDtoService userDtoService = new UserDtoService(propertyService, connectionService);

    @NotNull
    @Getter
    private final IProjectTaskDtoService projectTaskDtoService = new ProjectTaskDtoService(connectionService);

    @Getter
    @NotNull
    private final ISessionDtoService sessionDtoService = new SessionDtoService(connectionService);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(propertyService, connectionService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userDtoService, sessionDtoService);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    {
        registry(authEndpoint);
        registry(systemEndpoint);
        registry(userEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);

    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = getPropertyService().getServerHost();
        @NotNull final Integer port = getPropertyService().getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    public void start() {
        initPID();
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
    }

}