package ru.kuzin.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.api.repository.model.IRepository;
import ru.kuzin.tm.enumerated.Sort;
import ru.kuzin.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    final EntityManager entityManager;

    public AbstractRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    protected abstract Class<M> getEntityClass();

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public void clear() {
        for (final M model : findAll())
            entityManager.remove(model);
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Override
    @NotNull
    public List<M> findAll() {
        @NotNull String query = "SELECT m FROM " + getEntityClass().getSimpleName() + " m";
        return entityManager.createQuery(query, getEntityClass()).getResultList();
    }

    @Override
    @NotNull
    public List<M> findAll(@Nullable final Sort sort) {
        @NotNull String query = "SELECT m FROM " + getEntityClass().getSimpleName() + " m";
        @NotNull String orderStatement = " ORDER BY m." + (sort != null ? sort.getSortField() : Sort.BY_CREATED.getSortField());
        query += orderStatement;
        return entityManager.createQuery(query, getEntityClass()).getResultList();
    }

    @Override
    @Nullable
    public M findOneById(@NotNull final String id) {
        @NotNull String query = "SELECT m FROM " + getEntityClass().getSimpleName() + " m WHERE m.id = :id";
        return entityManager.createQuery(query, getEntityClass())
                .setParameter("id", id)
                .getResultStream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public M findOneByIndex(@NotNull final Integer index) {
        @NotNull String query = "SELECT m FROM " + getEntityClass().getSimpleName() + " m";
        return entityManager.createQuery(query, getEntityClass())
                .setMaxResults(index)
                .getResultStream()
                .skip(index - 1)
                .findFirst()
                .orElse(null);
    }

    @Override
    public int getSize() {
        @NotNull String query = "SELECT COUNT(m) FROM " + getEntityClass().getSimpleName() + " m";
        return entityManager.createQuery(query, Long.class).getSingleResult().intValue();
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

    @Override
    public void removeById(@NotNull final String id) {
        entityManager.remove(findOneById(id));
    }

    @Override
    public void update(@NotNull M model) {
        entityManager.merge(model);
    }

}