package ru.kuzin.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.api.repository.model.IUserOwnedRepository;
import ru.kuzin.tm.enumerated.Sort;
import ru.kuzin.tm.model.AbstractUserOwnedModel;

import javax.persistence.EntityManager;
import java.util.List;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> implements IUserOwnedRepository<M> {

    @NotNull
    final EntityManager entityManager;

    public AbstractUserOwnedRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    protected abstract Class<M> getEntityClass();

    @Override
    public void add(@NotNull final String userId, @NotNull final M model) {
        if (!userId.equals(model.getUser().getId())) model.getUser().setId(userId);
        entityManager.persist(model);
    }

    @Override
    public void clear(@NotNull final String userId) {
        for (final M model : findAll(userId))
            entityManager.remove(model);
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    @NotNull
    public List<M> findAll(@NotNull final String userId) {
        @NotNull String query = "SELECT m FROM " + getEntityClass().getSimpleName() + " m WHERE m.user.id = :userId";
        return entityManager.createQuery(query, getEntityClass()).setParameter("userId", userId).getResultList();
    }

    @Override
    @NotNull
    public List<M> findAll(@NotNull final String userId, @Nullable final Sort sort) {
        @NotNull String query = "SELECT m FROM " + getEntityClass().getSimpleName() + " m WHERE m.user.id = :userId";
        @NotNull String orderStatement = " ORDER BY m." + (sort != null ? sort.getSortField() : Sort.BY_CREATED.getSortField());
        query += orderStatement;
        return entityManager.createQuery(query, getEntityClass()).setParameter("userId", userId).getResultList();
    }

    @Override
    @Nullable
    public M findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull String query = "SELECT m FROM " + getEntityClass().getSimpleName() + " m WHERE m.user.id = :userId AND m.id = :id";
        return entityManager.createQuery(query, getEntityClass())
                .setParameter("userId", userId)
                .setParameter("id", id).getResultStream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public M findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull String query = "SELECT m FROM " + getEntityClass().getSimpleName() + " m WHERE m.user.id = :userId";
        return entityManager.createQuery(query, getEntityClass())
                .setParameter("userId", userId)
                .setMaxResults(index)
                .getResultStream()
                .skip(index - 1)
                .findFirst()
                .orElse(null);
    }

    @Override
    public int getSize(@NotNull final String userId) {
        @NotNull String query = "SELECT COUNT(m) FROM " + getEntityClass().getSimpleName() + " m WHERE m.user.id = :userId";
        return entityManager.createQuery(query, Long.class).setParameter("userId", userId).getSingleResult().intValue();
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final M model) {
        if (userId.equals(model.getUser().getId())) entityManager.remove(model);
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        entityManager.remove(findOneById(userId, id));
    }

    @Override
    public void update(@NotNull M model) {
        entityManager.merge(model);
    }

}