package ru.kuzin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.kuzin.tm.api.repository.ISessionRepository;
import ru.kuzin.tm.api.service.IConnectionService;
import ru.kuzin.tm.api.service.IPropertyService;
import ru.kuzin.tm.dto.model.SessionDTO;
import ru.kuzin.tm.exception.entity.EntityNotFoundException;
import ru.kuzin.tm.marker.UnitCategory;
import ru.kuzin.tm.service.ConnectionService;
import ru.kuzin.tm.service.PropertyService;

import java.sql.Connection;
import java.util.Collections;

import static ru.kuzin.tm.constant.SessionTestData.*;
import static ru.kuzin.tm.constant.UserTestData.ADMIN_TEST;
import static ru.kuzin.tm.constant.UserTestData.USER_TEST;

@Category(UnitCategory.class)
public final class SessionRepositoryTest {

    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final Connection connection = connectionService.getConnection();

    @NotNull
    private final ISessionRepository repository = new SessionRepository();

    @NotNull
    private final ISessionRepository emptyRepository = new SessionRepository();

    @Before
    public void before() throws Exception {
        repository.add(USER_SESSION1);
        repository.add(USER_SESSION2);
    }

    @After
    public void after() throws Exception {
        repository.removeAll(SESSION_LIST);
    }

    @Test(expected = NullPointerException.class)
    public void add() throws Exception {
        Assert.assertNull(repository.add(NULL_SESSION));
        Assert.assertNotNull(repository.add(ADMIN_SESSION1));
        @Nullable final SessionDTO session = repository.findOneById(ADMIN_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(ADMIN_SESSION1, session);
    }

    @Test
    public void addMany() throws Exception {
        Assert.assertNotNull(repository.add(ADMIN_SESSION_LIST));
        for (final SessionDTO session : ADMIN_SESSION_LIST)
            Assert.assertEquals(session, repository.findOneById(session.getId()));
    }

    @Test(expected = NullPointerException.class)
    public void addByUserId() throws Exception {
        Assert.assertNull(repository.add(ADMIN_TEST.getId(), NULL_SESSION));
        Assert.assertNull(repository.add(null, ADMIN_SESSION1));
        Assert.assertNotNull(repository.add(ADMIN_TEST.getId(), ADMIN_SESSION1));
        @Nullable final SessionDTO session = repository.findOneById(ADMIN_TEST.getId(), ADMIN_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(ADMIN_SESSION1, session);
    }

    @Test
    public void set() throws Exception {
        @NotNull final ISessionRepository emptyRepository = new SessionRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_SESSION_LIST);
        emptyRepository.set(ADMIN_SESSION_LIST);
        Assert.assertEquals(ADMIN_SESSION_LIST, emptyRepository.findAll());
    }

    @Test
    public void findAll() throws Exception {
        @NotNull final ISessionRepository emptyRepository = new SessionRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_SESSION_LIST);
        Assert.assertEquals(USER_SESSION_LIST, emptyRepository.findAll());
    }

    @Test
    public void findAllByUserId() throws Exception {
        Assert.assertEquals(Collections.emptyList(), repository.findAll(""));
        Assert.assertEquals(USER_SESSION_LIST, repository.findAll(USER_TEST.getId()));
    }

    @Test
    public void existsById() throws Exception {
        Assert.assertFalse(repository.existsById(NON_EXISTING_SESSION_ID));
        Assert.assertTrue(repository.existsById(USER_SESSION1.getId()));
    }

    @Test
    public void existsByIdByUserId() throws Exception {
        Assert.assertFalse(repository.existsById(USER_TEST.getId(), NON_EXISTING_SESSION_ID));
        Assert.assertTrue(repository.existsById(USER_TEST.getId(), USER_SESSION1.getId()));
    }

    @Test
    public void findOneById() throws Exception {
        Assert.assertNull(repository.findOneById(null));
        Assert.assertNull(repository.findOneById(NON_EXISTING_SESSION_ID));
        @Nullable final SessionDTO session = repository.findOneById(USER_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION1, session);
    }

    @Test
    public void findOneByIdByUserId() throws Exception {
        Assert.assertNull(repository.findOneById(USER_TEST.getId(), null));
        Assert.assertNull(repository.findOneById(USER_TEST.getId(), NON_EXISTING_SESSION_ID));
        @Nullable final SessionDTO session = repository.findOneById(USER_TEST.getId(), USER_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION1, session);
    }

    @Test(expected = NullPointerException.class)
    public void findOneByIndexByUserId() throws Exception {
        Assert.assertNull(repository.findOneByIndex(USER_TEST.getId(), null));
        final int index = repository.findAll().indexOf(USER_SESSION1);
        @Nullable final SessionDTO session = repository.findOneByIndex(USER_TEST.getId(), index);
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION1, session);
    }

    @Test
    public void clear() throws Exception {
        @NotNull final ISessionRepository emptyRepository = new SessionRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_SESSION_LIST);
        emptyRepository.clear();
        Assert.assertEquals(0, emptyRepository.getSize());
    }

    @Test
    public void clearByUserId() throws Exception {
        @NotNull final ISessionRepository emptyRepository = new SessionRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_SESSION1);
        emptyRepository.add(USER_SESSION2);
        emptyRepository.clear(USER_TEST.getId());
        Assert.assertEquals(0, emptyRepository.getSize(USER_TEST.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void remove() throws Exception {
        Assert.assertNull(repository.remove(null));
        @Nullable final SessionDTO createdSession = repository.add(ADMIN_SESSION1);
        @Nullable final SessionDTO removedSession = repository.remove(createdSession);
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(ADMIN_SESSION1, removedSession);
        Assert.assertNull(repository.findOneById(ADMIN_SESSION1.getId()));
    }

    @Test
    public void removeByUserId() throws Exception {
        Assert.assertNull(repository.remove(ADMIN_TEST.getId(), null));
        @Nullable final SessionDTO createdSession = repository.add(ADMIN_SESSION1);
        Assert.assertNull(repository.remove(null, createdSession));
        @Nullable final SessionDTO removedSession = repository.remove(ADMIN_TEST.getId(), createdSession);
        Assert.assertEquals(ADMIN_SESSION1, removedSession);
        Assert.assertNull(repository.findOneById(ADMIN_TEST.getId(), ADMIN_SESSION1.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void removeById() throws Exception {
        Assert.assertNull(repository.removeById(null));
        Assert.assertNull(repository.removeById(NON_EXISTING_SESSION_ID));
        @Nullable final SessionDTO createdSession = repository.add(ADMIN_SESSION1);
        @Nullable final SessionDTO removedSession = repository.removeById(ADMIN_SESSION1.getId());
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(ADMIN_SESSION1, removedSession);
        Assert.assertNull(repository.findOneById(ADMIN_SESSION1.getId()));
    }

    @Test
    public void removeByIdByUserId() throws Exception {
        Assert.assertNull(repository.removeById(ADMIN_TEST.getId(), null));
        Assert.assertNull(repository.removeById(ADMIN_TEST.getId(), NON_EXISTING_SESSION_ID));
        Assert.assertNull(repository.removeById(ADMIN_TEST.getId(), USER_SESSION1.getId()));
        @Nullable final SessionDTO createdSession = repository.add(ADMIN_SESSION1);
        Assert.assertNull(repository.removeById(null, createdSession.getId()));
        @Nullable final SessionDTO removedSession = repository.removeById(ADMIN_TEST.getId(), createdSession.getId());
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(ADMIN_SESSION1, removedSession);
        Assert.assertNull(repository.findOneById(ADMIN_TEST.getId(), ADMIN_SESSION1.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void removeByIndex() throws Exception {
        Assert.assertNull(repository.removeByIndex(null));
        @Nullable final SessionDTO createdSession = repository.add(ADMIN_SESSION1);
        final int index = repository.findAll().indexOf(createdSession);
        @Nullable final SessionDTO removedSession = repository.removeByIndex(index);
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(ADMIN_SESSION1, removedSession);
        Assert.assertNull(repository.findOneById(ADMIN_SESSION1.getId()));
    }

    @Test(expected = NullPointerException.class)
    public void removeByIndexByUserId() throws Exception {
        Assert.assertNull(repository.removeByIndex(ADMIN_TEST.getId(), null));
        @Nullable final SessionDTO createdSession = repository.add(ADMIN_SESSION1);
        final int index = repository.findAll(ADMIN_TEST.getId()).indexOf(createdSession);
        Assert.assertNull(repository.removeByIndex(null, index));
        @Nullable final SessionDTO removedSession = repository.removeByIndex(ADMIN_TEST.getId(), index);
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(ADMIN_SESSION1, removedSession);
        Assert.assertNull(repository.findOneById(ADMIN_TEST.getId(), ADMIN_SESSION1.getId()));
    }

    @Test
    public void getSize() throws Exception {
        @NotNull final ISessionRepository emptyRepository = new SessionRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        Assert.assertEquals(0, emptyRepository.getSize());
        emptyRepository.add(ADMIN_SESSION1);
        Assert.assertEquals(1, emptyRepository.getSize());
    }

    @Test
    public void getSizeByUserId() throws Exception {
        @NotNull final ISessionRepository emptyRepository = new SessionRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        Assert.assertEquals(0, emptyRepository.getSize(ADMIN_TEST.getId()));
        emptyRepository.add(ADMIN_SESSION1);
        emptyRepository.add(USER_SESSION1);
        Assert.assertEquals(1, emptyRepository.getSize(ADMIN_TEST.getId()));
    }

    @Test
    public void removeAll() throws Exception {
        @NotNull final ISessionRepository emptyRepository = new SessionRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(SESSION_LIST);
        emptyRepository.removeAll(SESSION_LIST);
        Assert.assertEquals(0, emptyRepository.getSize());
    }

}