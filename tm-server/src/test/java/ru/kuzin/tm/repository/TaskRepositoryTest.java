package ru.kuzin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.kuzin.tm.api.repository.ITaskRepository;
import ru.kuzin.tm.api.service.IConnectionService;
import ru.kuzin.tm.api.service.IPropertyService;
import ru.kuzin.tm.comparator.NameComparator;
import ru.kuzin.tm.dto.model.TaskDTO;
import ru.kuzin.tm.exception.entity.EntityNotFoundException;
import ru.kuzin.tm.marker.UnitCategory;
import ru.kuzin.tm.service.ConnectionService;
import ru.kuzin.tm.service.PropertyService;

import java.sql.Connection;
import java.util.Collections;
import java.util.Comparator;
import java.util.stream.Collectors;

import static ru.kuzin.tm.constant.ProjectTestData.USER_PROJECT1;
import static ru.kuzin.tm.constant.TaskTestData.*;
import static ru.kuzin.tm.constant.UserTestData.ADMIN_TEST;
import static ru.kuzin.tm.constant.UserTestData.USER_TEST;

@Category(UnitCategory.class)
public final class TaskRepositoryTest {

    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final Connection connection = connectionService.getConnection();

    @NotNull
    private final ITaskRepository repository = new TaskRepository();

    @NotNull
    private final ITaskRepository emptyRepository = new TaskRepository();

    @Before
    public void before() throws Exception {
        repository.add(USER_TASK1);
        repository.add(USER_TASK2);
    }

    @After
    public void after() throws Exception {
        repository.removeAll(TASK_LIST);
    }

    @Test(expected = NullPointerException.class)
    public void add() throws Exception {
        Assert.assertNull(repository.add(NULL_TASK));
        Assert.assertNotNull(repository.add(ADMIN_TASK1));
        @Nullable final TaskDTO task = repository.findOneById(ADMIN_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(ADMIN_TASK1, task);
    }

    @Test
    public void addMany() throws Exception {
        Assert.assertNotNull(repository.add(ADMIN_TASK_LIST));
        for (final TaskDTO task : ADMIN_TASK_LIST)
            Assert.assertEquals(task, repository.findOneById(task.getId()));
    }

    @Test(expected = NullPointerException.class)
    public void addByUserId() throws Exception {
        Assert.assertNull(repository.add(ADMIN_TEST.getId(), NULL_TASK));
        Assert.assertNull(repository.add(null, ADMIN_TASK1));
        Assert.assertNotNull(repository.add(ADMIN_TEST.getId(), ADMIN_TASK1));
        @Nullable final TaskDTO task = repository.findOneById(ADMIN_TEST.getId(), ADMIN_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(ADMIN_TASK1, task);
    }

    @Test
    public void set() throws Exception {
        @NotNull final ITaskRepository emptyRepository = new TaskRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_TASK_LIST);
        emptyRepository.set(ADMIN_TASK_LIST);
        Assert.assertEquals(ADMIN_TASK_LIST, emptyRepository.findAll());
    }

    @Test
    public void findAll() throws Exception {
        @NotNull final ITaskRepository emptyRepository = new TaskRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_TASK_LIST);
        Assert.assertEquals(USER_TASK_LIST, emptyRepository.findAll());
    }

    @Test
    public void findAllByUserId() throws Exception {
        Assert.assertEquals(Collections.emptyList(), repository.findAll(""));
        Assert.assertEquals(USER_TASK_LIST, repository.findAll(USER_TEST.getId()));
    }

    @Test
    public void findAllComparator() throws Exception {
        @NotNull final ITaskRepository emptyRepository = new TaskRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_TASK_LIST);
        emptyRepository.add(ADMIN_TASK_LIST);
        @NotNull final Comparator comparator = NameComparator.INSTANCE;
        Assert.assertEquals(SORTED_TASK_LIST, emptyRepository.findAll(comparator));
    }

    @Test
    public void findAllComparatorByUserId() throws Exception {
        @NotNull final Comparator comparator = NameComparator.INSTANCE;
        Assert.assertEquals(USER_TASK_LIST.stream().sorted(comparator).collect(Collectors.toList()), repository.findAll(USER_TEST.getId(), comparator));
    }

    @Test
    public void existsById() throws Exception {
        Assert.assertFalse(repository.existsById(NON_EXISTING_TASK_ID));
        Assert.assertTrue(repository.existsById(USER_TASK1.getId()));
    }

    @Test
    public void existsByIdByUserId() throws Exception {
        Assert.assertFalse(repository.existsById(USER_TEST.getId(), NON_EXISTING_TASK_ID));
        Assert.assertTrue(repository.existsById(USER_TEST.getId(), USER_TASK1.getId()));
    }

    @Test
    public void findOneById() throws Exception {
        Assert.assertNull(repository.findOneById(null));
        Assert.assertNull(repository.findOneById(NON_EXISTING_TASK_ID));
        @Nullable final TaskDTO task = repository.findOneById(USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1, task);
    }

    @Test
    public void findOneByIdByUserId() throws Exception {
        Assert.assertNull(repository.findOneById(USER_TEST.getId(), null));
        Assert.assertNull(repository.findOneById(USER_TEST.getId(), NON_EXISTING_TASK_ID));
        @Nullable final TaskDTO task = repository.findOneById(USER_TEST.getId(), USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1, task);
    }

    @Test(expected = NullPointerException.class)
    public void findOneByIndexByUserId() throws Exception {
        Assert.assertNull(repository.findOneByIndex(USER_TEST.getId(), null));
        final int index = repository.findAll().indexOf(USER_TASK1);
        @Nullable final TaskDTO task = repository.findOneByIndex(USER_TEST.getId(), index);
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1, task);
    }

    @Test
    public void findAllByProjectId() throws Exception {
        Assert.assertEquals(USER_TASK_LIST, repository.findAllByProjectId(USER_TEST.getId(), USER_PROJECT1.getId()));
    }

    @Test
    public void clear() throws Exception {
        @NotNull final ITaskRepository emptyRepository = new TaskRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_TASK_LIST);
        emptyRepository.clear();
        Assert.assertEquals(0, emptyRepository.getSize());
    }

    @Test
    public void clearByUserId() throws Exception {
        @NotNull final ITaskRepository emptyRepository = new TaskRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_TASK1);
        emptyRepository.add(USER_TASK2);
        emptyRepository.clear(USER_TEST.getId());
        Assert.assertEquals(0, emptyRepository.getSize(USER_TEST.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void remove() throws Exception {
        Assert.assertNull(repository.remove(null));
        @Nullable final TaskDTO createdTask = repository.add(ADMIN_TASK1);
        @Nullable final TaskDTO removedTask = repository.remove(createdTask);
        Assert.assertNotNull(removedTask);
        Assert.assertEquals(ADMIN_TASK1, removedTask);
        Assert.assertNull(repository.findOneById(ADMIN_TASK1.getId()));
    }

    @Test
    public void removeByUserId() throws Exception {
        Assert.assertNull(repository.remove(ADMIN_TEST.getId(), null));
        @Nullable final TaskDTO createdTask = repository.add(ADMIN_TASK1);
        Assert.assertNull(repository.remove(null, createdTask));
        @Nullable final TaskDTO removedTask = repository.remove(ADMIN_TEST.getId(), createdTask);
        Assert.assertEquals(ADMIN_TASK1, removedTask);
        Assert.assertNull(repository.findOneById(ADMIN_TEST.getId(), ADMIN_TASK1.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void removeById() throws Exception {
        Assert.assertNull(repository.removeById(null));
        Assert.assertNull(repository.removeById(NON_EXISTING_TASK_ID));
        @Nullable final TaskDTO createdTask = repository.add(ADMIN_TASK1);
        @Nullable final TaskDTO removedTask = repository.removeById(ADMIN_TASK1.getId());
        Assert.assertNotNull(removedTask);
        Assert.assertEquals(ADMIN_TASK1, removedTask);
        Assert.assertNull(repository.findOneById(ADMIN_TASK1.getId()));
    }

    @Test
    public void removeByIdByUserId() throws Exception {
        Assert.assertNull(repository.removeById(ADMIN_TEST.getId(), null));
        Assert.assertNull(repository.removeById(ADMIN_TEST.getId(), NON_EXISTING_TASK_ID));
        Assert.assertNull(repository.removeById(ADMIN_TEST.getId(), USER_TASK1.getId()));
        @Nullable final TaskDTO createdTask = repository.add(ADMIN_TASK1);
        Assert.assertNull(repository.removeById(null, createdTask.getId()));
        @Nullable final TaskDTO removedTask = repository.removeById(ADMIN_TEST.getId(), createdTask.getId());
        Assert.assertNotNull(removedTask);
        Assert.assertEquals(ADMIN_TASK1, removedTask);
        Assert.assertNull(repository.findOneById(ADMIN_TEST.getId(), ADMIN_TASK1.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void removeByIndex() throws Exception {
        Assert.assertNull(repository.removeByIndex(null));
        @Nullable final TaskDTO createdTask = repository.add(ADMIN_TASK1);
        final int index = repository.findAll().indexOf(createdTask);
        @Nullable final TaskDTO removedTask = repository.removeByIndex(index);
        Assert.assertNotNull(removedTask);
        Assert.assertEquals(ADMIN_TASK1, removedTask);
        Assert.assertNull(repository.findOneById(ADMIN_TASK1.getId()));
    }

    @Test(expected = NullPointerException.class)
    public void removeByIndexByUserId() throws Exception {
        Assert.assertNull(repository.removeByIndex(ADMIN_TEST.getId(), null));
        @Nullable final TaskDTO createdTask = repository.add(ADMIN_TASK1);
        final int index = repository.findAll(ADMIN_TEST.getId()).indexOf(createdTask);
        Assert.assertNull(repository.removeByIndex(null, index));
        @Nullable final TaskDTO removedTask = repository.removeByIndex(ADMIN_TEST.getId(), index);
        Assert.assertNotNull(removedTask);
        Assert.assertEquals(ADMIN_TASK1, removedTask);
        Assert.assertNull(repository.findOneById(ADMIN_TEST.getId(), ADMIN_TASK1.getId()));
    }

    @Test
    public void getSize() throws Exception {
        @NotNull final ITaskRepository emptyRepository = new TaskRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        Assert.assertEquals(0, emptyRepository.getSize());
        emptyRepository.add(ADMIN_TASK1);
        Assert.assertEquals(1, emptyRepository.getSize());
    }

    @Test
    public void getSizeByUserId() throws Exception {
        @NotNull final ITaskRepository emptyRepository = new TaskRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        Assert.assertEquals(0, emptyRepository.getSize(ADMIN_TEST.getId()));
        emptyRepository.add(ADMIN_TASK1);
        emptyRepository.add(USER_TASK1);
        Assert.assertEquals(1, emptyRepository.getSize(ADMIN_TEST.getId()));
    }

    @Test
    public void removeAll() throws Exception {
        @NotNull final ITaskRepository emptyRepository = new TaskRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(TASK_LIST);
        emptyRepository.removeAll(TASK_LIST);
        Assert.assertEquals(0, emptyRepository.getSize());
    }

}