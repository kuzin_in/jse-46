package ru.kuzin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.kuzin.tm.api.repository.ISessionRepository;
import ru.kuzin.tm.api.service.IConnectionService;
import ru.kuzin.tm.api.service.IPropertyService;
import ru.kuzin.tm.api.service.dto.ISessionDtoService;
import ru.kuzin.tm.dto.model.SessionDTO;
import ru.kuzin.tm.exception.entity.EntityNotFoundException;
import ru.kuzin.tm.exception.field.IdEmptyException;
import ru.kuzin.tm.exception.field.IndexIncorrectException;
import ru.kuzin.tm.exception.field.UserIdEmptyException;
import ru.kuzin.tm.marker.UnitCategory;
import ru.kuzin.tm.repository.SessionRepository;

import java.sql.Connection;

import static ru.kuzin.tm.constant.SessionTestData.*;
import static ru.kuzin.tm.constant.UserTestData.ADMIN_TEST;
import static ru.kuzin.tm.constant.UserTestData.USER_TEST;

@Category(UnitCategory.class)
public final class SessionServiceTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final Connection connection = connectionService.getConnection();

    @NotNull
    private final ISessionRepository repository = new SessionRepository();

    @NotNull
    private final ISessionDtoService service = new SessionService(repository, connectionService);

    private final ISessionDtoService emptyService = new SessionService(repository, connectionService);

    @Before
    public void before() throws Exception {
        repository.add(USER_SESSION1);
        repository.add(USER_SESSION2);
    }

    @After
    public void after() throws Exception {
        repository.removeAll(SESSION_LIST);
    }

    @Test
    public void add() throws Exception {
        Assert.assertNull(service.add(NULL_SESSION));
        Assert.assertNotNull(service.add(ADMIN_SESSION1));
        @Nullable final SessionDTO session = service.findOneById(ADMIN_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(ADMIN_SESSION1, session);
    }

    @Test
    public void addByUserId() throws Exception {
        Assert.assertNull(service.add(ADMIN_TEST.getId(), NULL_SESSION));
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.add(null, ADMIN_SESSION1);
        });
        Assert.assertNotNull(service.add(ADMIN_TEST.getId(), ADMIN_SESSION1));
        @Nullable final SessionDTO session = service.findOneById(ADMIN_TEST.getId(), ADMIN_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(ADMIN_SESSION1, session);
    }

    @Test
    public void addMany() throws Exception {
        Assert.assertNotNull(service.add(ADMIN_SESSION_LIST));
        for (final SessionDTO session : ADMIN_SESSION_LIST)
            Assert.assertEquals(session, service.findOneById(session.getId()));
    }

    @Test
    public void set() throws Exception {
        @NotNull final ISessionRepository emptyRepository = new SessionRepository();
        @NotNull final ISessionDtoService emptyService = new SessionService(emptyRepository, connectionService);
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(USER_SESSION_LIST);
        emptyService.set(ADMIN_SESSION_LIST);
        Assert.assertEquals(ADMIN_SESSION_LIST, emptyService.findAll());
    }

    @Test
    public void findAll() throws Exception {
        @NotNull final ISessionRepository emptyRepository = new SessionRepository();
        @NotNull final ISessionDtoService emptyService = new SessionService(emptyRepository, connectionService);
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(USER_SESSION_LIST);
        Assert.assertEquals(USER_SESSION_LIST, emptyService.findAll());
    }

    @Test
    public void findAllByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findAll("");
        });
        Assert.assertEquals(USER_SESSION_LIST, service.findAll(USER_TEST.getId()));
    }

    @Test
    public void existsById() throws Exception {
        Assert.assertFalse(service.existsById(""));
        Assert.assertFalse(service.existsById(null));
        Assert.assertFalse(service.existsById(NON_EXISTING_SESSION_ID));
        Assert.assertTrue(service.existsById(USER_SESSION1.getId()));
    }

    @Test(expected = IdEmptyException.class)
    public void existsByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById(null, NON_EXISTING_SESSION_ID);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById("", NON_EXISTING_SESSION_ID);
        });
        Assert.assertFalse(service.existsById(USER_TEST.getId(), null));
        Assert.assertFalse(service.existsById(USER_TEST.getId(), ""));
        Assert.assertFalse(service.existsById(USER_TEST.getId(), NON_EXISTING_SESSION_ID));
        Assert.assertTrue(service.existsById(USER_TEST.getId(), USER_SESSION1.getId()));
    }

    @Test
    public void findOneById() throws Exception {
        Assert.assertNull(service.findOneById(NON_EXISTING_SESSION_ID));
        @Nullable final SessionDTO session = service.findOneById(USER_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION1, session);
    }

    @Test
    public void findOneByIdByUserId() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById(USER_TEST.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById(USER_TEST.getId(), "");
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById(null, USER_SESSION1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById("", USER_SESSION1.getId());
        });
        Assert.assertNull(service.findOneById(USER_TEST.getId(), NON_EXISTING_SESSION_ID));
        @Nullable final SessionDTO session = service.findOneById(USER_TEST.getId(), USER_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION1, session);
    }

    @Test
    public void findOneByIndexByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findOneByIndex(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findOneByIndex("", null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.findOneByIndex(USER_TEST.getId(), null);
        });
        Assert.assertThrows(IllegalArgumentException.class, () -> {
            service.findOneByIndex(USER_TEST.getId(), -1);
        });
        final int index = service.findAll().indexOf(USER_SESSION1);
        @Nullable final SessionDTO session = service.findOneByIndex(USER_TEST.getId(), index);
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION1, session);
    }

    @Test
    public void clear() throws Exception {
        @NotNull final ISessionRepository emptyRepository = new SessionRepository();
        @NotNull final ISessionDtoService emptyService = new SessionService(emptyRepository, connectionService);
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(USER_SESSION_LIST);
        emptyService.clear();
        Assert.assertEquals(0, emptyService.getSize());
    }

    @Test
    public void clearByUserId() throws Exception {
        @NotNull final ISessionRepository emptyRepository = new SessionRepository();
        @NotNull final ISessionDtoService emptyService = new SessionService(emptyRepository, connectionService);
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            emptyService.clear(null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            emptyService.clear("");
        });
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(USER_SESSION1);
        emptyService.add(USER_SESSION2);
        emptyService.clear(USER_TEST.getId());
        Assert.assertEquals(0, emptyService.getSize(USER_TEST.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void remove() throws Exception {
        Assert.assertNull(service.remove(null));
        @Nullable final SessionDTO createdSession = service.add(ADMIN_SESSION1);
        @Nullable final SessionDTO removedSession = service.remove(createdSession);
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(ADMIN_SESSION1, removedSession);
        Assert.assertNull(service.findOneById(ADMIN_SESSION1.getId()));
    }

    @Test
    public void removeByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.remove(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.remove("", null);
        });
        Assert.assertNull(service.remove(ADMIN_TEST.getId(), null));
        @Nullable final SessionDTO createdSession = service.add(ADMIN_SESSION1);
        @Nullable final SessionDTO removedSession = service.remove(ADMIN_TEST.getId(), createdSession);
        Assert.assertEquals(ADMIN_SESSION1, removedSession);
        Assert.assertNull(service.findOneById(ADMIN_TEST.getId(), ADMIN_SESSION1.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void removeById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById("");
        });
        Assert.assertNull(service.removeById(NON_EXISTING_SESSION_ID));
        @Nullable final SessionDTO createdSession = service.add(ADMIN_SESSION1);
        @Nullable final SessionDTO removedSession = service.removeById(ADMIN_SESSION1.getId());
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(ADMIN_SESSION1, removedSession);
        Assert.assertNull(service.findOneById(ADMIN_SESSION1.getId()));
    }

    @Test
    public void removeByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeById(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeById("", null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(USER_TEST.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(USER_TEST.getId(), "");
        });
        Assert.assertNull(service.removeById(ADMIN_TEST.getId(), NON_EXISTING_SESSION_ID));
        Assert.assertNull(service.removeById(ADMIN_TEST.getId(), USER_SESSION1.getId()));
        @Nullable final SessionDTO createdSession = service.add(ADMIN_SESSION1);
        @Nullable final SessionDTO removedSession = service.removeById(ADMIN_TEST.getId(), createdSession.getId());
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(ADMIN_SESSION1, removedSession);
        Assert.assertNull(service.findOneById(ADMIN_TEST.getId(), ADMIN_SESSION1.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void removeByIndex() throws Exception {
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.removeByIndex(null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.removeByIndex(-1);
        });
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            service.removeByIndex(service.getSize());
        });
        @Nullable final SessionDTO createdSession = service.add(ADMIN_SESSION1);
        final int index = service.findAll().indexOf(createdSession);
        @Nullable final SessionDTO removedSession = service.removeByIndex(index);
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(ADMIN_SESSION1, removedSession);
        Assert.assertNull(service.findOneById(ADMIN_SESSION1.getId()));
    }

    @Test
    public void removeByIndexByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeByIndex(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeByIndex("", null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.removeByIndex(USER_TEST.getId(), null);
        });
        Assert.assertThrows(IllegalArgumentException.class, () -> {
            service.removeByIndex(USER_TEST.getId(), -1);
        });
        @Nullable final SessionDTO createdSession = service.add(ADMIN_SESSION1);
        final int index = service.findAll(ADMIN_TEST.getId()).indexOf(createdSession);
        @Nullable final SessionDTO removedSession = service.removeByIndex(ADMIN_TEST.getId(), index);
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(ADMIN_SESSION1, removedSession);
        Assert.assertNull(service.findOneById(ADMIN_TEST.getId(), ADMIN_SESSION1.getId()));
    }

    @Test
    public void getSize() throws Exception {
        @NotNull final ISessionRepository emptyRepository = new SessionRepository();
        @NotNull final ISessionDtoService emptyService = new SessionService(emptyRepository, connectionService);
        Assert.assertTrue(emptyService.findAll().isEmpty());
        Assert.assertEquals(0, emptyService.getSize());
        emptyService.add(ADMIN_SESSION1);
        Assert.assertEquals(1, emptyService.getSize());
    }

    @Test
    public void getSizeByUserId() throws Exception {
        @NotNull final ISessionRepository emptyRepository = new SessionRepository();
        @NotNull final ISessionDtoService emptyService = new SessionService(emptyRepository, connectionService);
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            emptyService.getSize(null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            emptyService.getSize("");
        });
        Assert.assertTrue(emptyService.findAll().isEmpty());
        Assert.assertEquals(0, emptyService.getSize(ADMIN_TEST.getId()));
        emptyService.add(ADMIN_SESSION1);
        emptyService.add(USER_SESSION1);
        Assert.assertEquals(1, emptyService.getSize(ADMIN_TEST.getId()));
    }

    @Test
    public void removeAll() throws Exception {
        @NotNull final ISessionRepository emptyRepository = new SessionRepository();
        @NotNull final ISessionDtoService emptyService = new SessionService(emptyRepository, connectionService);
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(SESSION_LIST);
        emptyService.removeAll(SESSION_LIST);
        Assert.assertEquals(0, emptyService.getSize());
    }

}